import * as firebase from 'firebase';
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

// export const environment = {
//   production: false,
//   firebase: {
//     apiKey: 'AIzaSyD8yyEhpoBqP02THW7YajGrq6a0NWowNus',
//     authDomain: 'angular-uczelnia.firebaseapp.com',
//     databaseURL: 'https://angular-uczelnia.firebaseio.com',
//     projectId: 'angular-uczelnia',
//     storageBucket: 'angular-uczelnia.appspot.com',
//     messagingSenderId: '929451601865'
//   }
// };

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyD8yyEhpoBqP02THW7YajGrq6a0NWowNus',
    authDomain: 'angular-uczelnia.firebaseapp.com',
    databaseURL: 'https://angular-uczelnia.firebaseio.com',
    projectId: 'angular-uczelnia',
    storageBucket: 'angular-uczelnia.appspot.com',
    messagingSenderId: '929451601865'
  }
};
