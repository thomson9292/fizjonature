// tslint:disable-next-line:no-empty-interface
export interface Icomment {
  galleryId: string;
  commentId: string;
  dateCreated: Date;
  firstName: string;
  lastName: string;
  email: string;
  message: string;
}
