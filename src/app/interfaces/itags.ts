export interface Itags {
  tagId: string;
  display: string;
  value: string;
  color: string;
}
