import { Upload } from './upload';

export interface Inews {
  newsId: string;
  title: string;
  dateCreated: string;
  thumbUrl: string;
  description: string;
  descriptionShort: string;
  photos?: Upload[];
}
