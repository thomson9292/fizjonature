// import {IPhoto} from './iphoto';
import { Upload } from './upload';

export interface IGallery {
  galleryId: string;
  title: string;
  dateCreated: string;
  thumbUrl: string;
  description: string;
  tags?: any;
  photos: Upload[];
}
