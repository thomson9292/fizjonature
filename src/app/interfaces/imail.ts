export interface Imail {
  mailId: string;
  dateCreated: Date;
  name: string;
  surname: string;
  userEmail: string;
  subject: string;
  text: string;
}
