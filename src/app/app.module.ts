import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { environment } from '../environments/environment';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { NgxGalleryModule } from 'ngx-gallery';
import { AgmCoreModule } from '@agm/core';
import { TagInputModule } from 'ngx-chips';
import { ColorPickerModule } from 'ngx-color-picker';

import { AppComponent } from './app.component';
import { PolishDatePipe } from './pipes/polish-date.pipe';
import { SearchGalleriesPipe } from './pipes/search-galleries.pipe';
import { OrderModule } from 'ngx-order-pipe';
import { DatePipe } from '@angular/common';
import { FilterByDatePipe } from './pipes/filter-by-date.pipe';
import { NavComponent } from './components/nav/nav.component';
import { GalleriesComponent } from './components/galleries/galleries.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { GalleriesSearchComponent } from './components/galleries-search/galleries-search.component';
import { YearFilterComponent } from './components/year-filter/year-filter.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { GalleryDetailsComponent } from './components/gallery-details/gallery-details.component';
import { LastYearService } from './services/last-year.service';
import { ContactComponent } from './components/contact/contact.component';
import { CommentFormComponent } from './components/comment-form/comment-form.component';
import { CommentsComponent } from './components/comments/comments.component';
import { ObjectValuesPipe } from './pipes/object-values.pipe';
import { FilterCommentsPipe } from './pipes/filter-comments.pipe';
import { PageViewPipe } from './pipes/page-view.pipe';
import { GalleryAddComponent } from './components/gallery-add/gallery-add.component';
import { GalleriesService } from './services/galleries.service';
// import { ToasterAppService } from './services/toaster.service';
import { CommentsService } from './services/comments.service';
import { PersonalComponent } from './components/personal-app/personal/personal.component';
import { NewsComponent } from './components/news-app/news/news.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireModule } from 'angularfire2';
// import { AngularFireDatabaseModule } from 'angularfire2/database';
// import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database-deprecated';
import * as firebase from 'firebase';
import { AdminComponent } from './components/admin/admin.component';
import { GalleryEditComponent } from './components/gallery-edit/gallery-edit.component';
import { NewsAddComponent } from './components/news-app/news-add/news-add.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { PersonalDetailsComponent } from './components/personal-app/personal-details/personal-details.component';
import { PriceListComponent } from './components/price-list/price-list.component';
import { FaqComponent } from './components/faq/faq.component';
import { MailListComponent } from './components/mail-list/mail-list.component';
import { CommentListComponent } from './components/comment-list/comment-list.component';
import { OfferComponent } from './components/offer-app/offer/offer.component';
import { OfferDetailsComponent } from './components/offer-app/offer-details/offer-details.component';
import { MainPhotoSliderComponent } from './components/main-photo-slider/main-photo-slider.component';
import { FooterComponent } from './components/footer/footer.component';
import { TagsColorComponent } from './components/tags-color/tags-color.component';
import { ConfirmationComponent } from './components/confirmation/confirmation.component';


firebase.initializeApp(environment.firebase);

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  // Change this to your upload POST address:
   url: 'https://httpbin.org/post',
   maxFilesize: 50,
   acceptedFiles: 'image/*'
 };

@NgModule({
  declarations: [
    AppComponent,
    PolishDatePipe,
    SearchGalleriesPipe,
    FilterByDatePipe,
    NavComponent,
    GalleriesComponent,
    GalleryComponent,
    GalleriesSearchComponent,
    YearFilterComponent,
    DashboardComponent,
    GalleryDetailsComponent,
    ContactComponent,
    CommentFormComponent,
    CommentsComponent,
    ObjectValuesPipe,
    FilterCommentsPipe,
    PageViewPipe,
    GalleryAddComponent,
    PersonalComponent,
    NewsComponent,
    LogInComponent,
    AdminComponent,
    GalleryEditComponent,
    NewsAddComponent,
    PaginationComponent,
    PersonalDetailsComponent,
    PriceListComponent,
    FaqComponent,
    MailListComponent,
    CommentListComponent,
    OfferComponent,
    OfferDetailsComponent,
    MainPhotoSliderComponent,
    FooterComponent,
    TagsColorComponent,
    ConfirmationComponent
  ],
  imports: [
    // AngularFireModule.initializeApp(environment.firebase, 'angularfs'),
    BrowserModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    CommonModule,
    ToastrModule.forRoot(),
    ReactiveFormsModule,
    TagInputModule,
    FormsModule,
    OrderModule,
    AppRoutingModule,
    HttpClientModule,
    ColorPickerModule,
    // AngularFireDatabaseModule,
    // AngularFireDatabase,
    // FirebaseListObservable,
    // FirebaseObjectObservable,
    // AngularFireAuthModule
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebase),
    PerfectScrollbarModule,
    DropzoneModule,
    NgxGalleryModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD8yyEhpoBqP02THW7YajGrq6a0NWowNus'
    })
  ],
  providers: [DatePipe, LastYearService, GalleriesService,
    // ToasterAppService,
    CommentsService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }, {
      provide: DROPZONE_CONFIG,
      useValue: DEFAULT_DROPZONE_CONFIG
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
