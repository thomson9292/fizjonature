import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterComments'
})
export class FilterCommentsPipe implements PipeTransform {

  comments = [];

  transform(value: any, args?: any): any {

    this.comments = value;
    // console.log(this.comments);

    this.comments = this.comments.filter(item =>
      (item.galleryId === args));

    // console.log(this.comments);
    return this.comments;
  }

}
