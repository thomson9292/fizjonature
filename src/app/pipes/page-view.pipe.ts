import { Pipe, PipeTransform } from '@angular/core';
import { GalleriesComponent } from '../components/galleries/galleries.component';
import { NumberFormatStyle } from '@angular/common';

@Pipe({
  name: 'pageView'
})
export class PageViewPipe implements PipeTransform {

  transform(value: any, pageLength: any, currentPage: number): any {
    // console.log(value, pageLength, currentPage);
    if (pageLength < 5) {
      return value + 1;
    } else {
      if (currentPage === 0) {
        if (value < 5) {
          return value + 1;
        } else {
          return null;
        }
      } else if (currentPage === 1) {
        if (value > -1 && value < 5) {
          return value + 1;
        } else {
          return null;
        }
      } else if (currentPage === pageLength - 2) {
        if (value > pageLength - 6 && value < pageLength) {
          return value + 1;
        } else {
          return null;
        }
      } else if (currentPage === pageLength - 1) {
        if (value > pageLength - 6) {
          return value + 1;
        } else {
          return null;
        }
      } else {
        if (value > currentPage - 3 && value < currentPage + 3) {
          return value + 1;
        } else {
          return null;
        }
      }
    }
  }
}
