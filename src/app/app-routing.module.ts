import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { GalleriesComponent } from './components/galleries/galleries.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { GalleryDetailsComponent } from './components/gallery-details/gallery-details.component';
import { ContactComponent } from './components/contact/contact.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { PersonalDetailsComponent } from './components/personal-app/personal-details/personal-details.component';
import { PriceListComponent } from './components/price-list/price-list.component';
import { FaqComponent } from './components/faq/faq.component';
import { OfferComponent } from './components/offer-app/offer/offer.component';

const routes: Routes = [{
  path: 'blog',
  component: GalleriesComponent
}, {
  path: 'home',
  component: DashboardComponent
}, {
  path: 'admin',
  component: LogInComponent
}, {
  path: '',
  redirectTo: '/home',
  pathMatch: 'full'
}, {
  path: 'blog/:galleryId',
  component: GalleryDetailsComponent
}, {
  path: 'kontakt',
  component: ContactComponent
}, {
  path: 'omnie',
  component: PersonalDetailsComponent
}, {
  path: 'cennik',
  component: PriceListComponent
}, {
  path: 'faq',
  component: FaqComponent
}, {
  path: 'oferta',
  component: OfferComponent
}
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)],
})
export class AppRoutingModule { }
