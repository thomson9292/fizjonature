import { Component, OnInit } from '@angular/core';
import { GalleriesService } from '../../services/galleries.service';
import { Icomment } from '../../interfaces/icomment';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.scss']
})
export class CommentListComponent implements OnInit {

  comments: Icomment[] = [];

  // pagination
  dataToChild;
  itemsPerPage = 3;
  start: number;
  end: number;

  constructor(private galleriesService: GalleriesService) { }

  // Pobranie komentarzy
  getComments() {
    this.galleriesService.getComments('comments')
      .subscribe(
        data => {
          console.log(data);
          this.comments = data;
          this.dataToChild = { items: this.comments, itemsPerPage: this.itemsPerPage };
        }
      );
  }

  getPageData(event) {
    this.start = event.start;
    this.end = event.end;
  }

  ngOnInit() {
    this.getComments();
  }

}
