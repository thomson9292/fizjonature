import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'year-filter',
  templateUrl: './year-filter.component.html',
  styleUrls: ['./year-filter.component.scss']
})
export class YearFilterComponent implements OnInit {

  year: any;

  @Input() years: any;

  @Output()
  choosenYear: EventEmitter<String> = new EventEmitter<String>();

  constructor() { }

  yearChanged(event) {
    this.choosenYear.emit(event);
  }

  ngOnInit() {
  }

}
