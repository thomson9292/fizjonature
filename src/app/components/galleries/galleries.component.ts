import { Component, OnInit } from '@angular/core';
import GALLERY from '../../constants/galleries';
// import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GalleriesService } from '../../services/galleries.service';
import * as $ from 'jquery';
import 'rxjs/add/operator/toPromise';
import { ToasterAppService } from '../../services/toaster.service';
import { TagsService } from '../../services/tags.service';
import { Itags } from '../../interfaces/itags';
import { ConfirmationService } from '../../services/confirmation.service';
import 'rxjs/add/operator/take';

export class Gallery {
  id: number;
  title: string;
  dateCreated: string;
  thumbUrl: string;
  description: string;
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'galleries',
  templateUrl: './galleries.component.html',
  styleUrls: ['./galleries.component.scss'],
})
export class GalleriesComponent implements OnInit {

  // gallerie
  galleries: any = [];
  galleriesStatic: any = [];
  gallerySub;

  // filtrowanie
  years: Date[] = [];
  searchValue: string;
  yearValue: any;
  tagSeachValue: string;

  // stronnicowanie
  dataToChild;
  itemsPerPage = 9;
  start: number;
  end: number;

  // // formularz dodawania galerii
  showForm = false;
  galleryToEdit: any;

  // taglist
  uniqueTags: any[] = [];
  uniqueTagsColors: any[] = [];
  newUniqueTags: Itags[] = [];
  tagData;
  showTags = false;
  tagButton = 'Pokaż dostępne tagi';

  hover;

  constructor(private galleriesService: GalleriesService, private toastr: ToasterAppService, private tagsService: TagsService,
    private confirmationService: ConfirmationService) {

    this.searchValue = '';
    this.yearValue = '';
  }


  onToggle(event?) {
    if (this.showForm === false) {
      $('#navigation').css('visibility', 'hidden');
      $('.pop-outer').fadeIn('slow');
      $('body').css('overflow', 'hidden');
      this.galleryToEdit = event[1];
      this.showForm = true;
    } else {
      $('#navigation').css('visibility', 'visible');
      $('.pop-outer').fadeOut('slow');
      $('body').css('overflow', 'auto');
      this.showForm = false;
      this.galleryToEdit = null;
    }
  }


  // Tworzenie unikatowej listy z dostępnymi tagami
  setTags() {
    this.galleries.forEach(element => {
      element.tags.forEach(element2 => {
        if (this.uniqueTags.indexOf(element2['value']) === -1) {
          this.uniqueTags.push(element2['value']);
          this.uniqueTagsColors.push(element2['color']);
        }
      });
    });
    for (let i = 0; i < this.uniqueTags.length; i++) {
      if (typeof this.uniqueTags[i] === 'object') {
        return 0;
      } else {
        this.uniqueTags[i] = { value: this.uniqueTags[i], color: this.uniqueTagsColors[i] };
      }
    }
  }

  // odebranie eventEmmitera od komponentu do filtrowania rocznikami
  setSearchValue(event) {
    this.searchValue = event;
    this.galleries = this.galleriesStatic;
    // console.log(event);

    if (this.searchValue) {
      this.galleries = this.galleries.filter(item =>
        item.title.toLowerCase().indexOf(this.searchValue.toLowerCase()) !== -1 ||
        item.tags.some(element => {
          return element.display === this.searchValue.toLowerCase();
        }) !== false
      );

      this.dataToChild = { items: this.galleries, itemsPerPage: this.itemsPerPage };
    } else {
      this.galleries = this.galleriesStatic;
      this.dataToChild = { items: this.galleries, itemsPerPage: this.itemsPerPage };
    }
  }

  setTagSeachValue(event) {
    this.tagSeachValue = event;
  }

  // odebranie eventEmmitera od komponentu z wyszukiwarką
  setYearValue(event) {
    this.yearValue = event;
    this.filterByDate();
  }

  // filtrowanie galerii rocznikami
  filterByDate() {
    if (this.yearValue === 'Wszystkie galerie') {
      this.galleries = this.galleriesStatic;
    } else {
      this.galleries = this.galleriesStatic;
      this.galleries = this.galleries.filter(item =>
        item.dateCreated.indexOf(this.yearValue) !== -1);
    }
    this.dataToChild = { items: this.galleries, itemsPerPage: this.itemsPerPage };
  }

  // wyeksportowanie wszystkich galerii (GALLERY) na serwer
  exportGalleries() {
    this.confirmationService.toggleModalOn('Tagi z wyeksportowanych galerii nie będą wykorzystywane w "suggested tags", kontynuować?');
    this.confirmationService.ifRemoveChange.take(1)
      .subscribe(value => {
        if (value) {
          this.confirmationService.modalOn = false;
          this.confirmationService.ifRemove = false;

          GALLERY.forEach(gallery => {
            this.galleriesService.sendData(gallery, 'galleries').then((response) => {
              this.dataToChild = { items: this.galleries, itemsPerPage: this.itemsPerPage };
              this.setYearList();
              this.toastr.Success('Id: ' + response['galleryId'], 'Gallery exported!');
            }, (errResponse) => {
              this.toastr.Error('Something went wrong :' + errResponse, 'Error!');
            });
          });
        }
      });
  }


  // usuniecie wszystkich galerii
  removeGalleries(event) {
    this.confirmationService.toggleModalOn('Wszystkie galerie zostaną usunięte, kontynuować?');
    this.confirmationService.ifRemoveChange.take(1)
      .subscribe(value => {
        if (value) {
          this.confirmationService.modalOn = false;
          this.confirmationService.ifRemove = false;

          this.galleries.forEach(gallery => {
            console.log(gallery.galleryId);
            this.galleriesService.delSingleData(gallery.galleryId, 'galleries').then((response) => {
              this.dataToChild = { items: this.galleries, itemsPerPage: this.itemsPerPage };
              this.setYearList();
              this.uniqueTags = [];
              this.toastr.Success('Gallery removed. ', 'Done!');
            }, (errResponse) => {
              this.toastr.Error('Something went wrong : ' + errResponse.message, 'Error!');
            });
          });
        }
      });
  }


  // usuniecie pojedynczej galerii
  removeGallery(event) {
    this.confirmationService.toggleModalOn('Jestes pewny?');
    this.confirmationService.ifRemoveChange.subscribe(value => {
      console.log(value);
      if (value) {
        this.confirmationService.modalOn = false;
        this.confirmationService.ifRemove = false;
        this.galleriesService.delSingleData(event, 'galleries').toPromise().then((response) => {
          this.dataToChild = { items: this.galleries, itemsPerPage: this.itemsPerPage };
          this.setYearList();
          this.toastr.Success('Gallery removed!');
        }, (errResponse) => {
          this.toastr.Error('Something went wrong : ' + errResponse.message, 'Error!');
        });
      }
    });
  }

  // odkrycie listy dostepnych tagow
  showTagList() {
    if (this.showTags === true) {
      this.showTags = false;
      this.tagButton = 'Pokaż dostępne tagi';
    } else {
      this.showTags = true;
      this.tagButton = 'Ukryj tagi';
    }
  }


  getPageData(event) {
    this.start = event.start;
    this.end = event.end;
  }


  // generowanie listy roczników + usuwanie duplikatów
  setYearList(): any {
    this.galleries.forEach(element => {
      element = element['dateCreated'].slice(0, 4);
      if (this.years.indexOf(element) === -1) {
        this.years.push(element);
      }
    });
    this.years.sort();
  }

  // pobranie wszystkich galerii na strone
  getGalleries() {
    this.gallerySub = this.galleriesService.getGalleries('galleries')
      .subscribe(
        data => {
          // console.log(data);
          this.galleries = data;
          this.galleriesStatic = this.galleries;
          this.dataToChild = { items: this.galleries, itemsPerPage: this.itemsPerPage };
          this.setYearList();
          this.setTags();
          // this.gallerySub.unsubscribe();
        }
      );
  }


  ngOnInit() {
    this.getGalleries();
  }

}
