import { Component, OnInit } from '@angular/core';
import { Imail } from '../../interfaces/imail';
import { GalleriesService } from '../../services/galleries.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mail-list',
  templateUrl: './mail-list.component.html',
  styleUrls: ['./mail-list.component.scss']
})
export class MailListComponent implements OnInit {

  mails: Imail[] = [];

  // pagination
  dataToChild;
  itemsPerPage = 3;
  start: number;
  end: number;

  constructor(private galleriesService: GalleriesService) { }

  // Pobranie komentarzy do galerii
  getMails() {
    this.galleriesService.getComments('mails')
      .subscribe(
        data => {
          console.log(data);
          this.mails = data;
          this.dataToChild = { items: this.mails, itemsPerPage: this.itemsPerPage };
        }
      );
  }

  getPageData(event) {
    this.start = event.start;
    this.end = event.end;
  }

  ngOnInit() {
    this.getMails();
  }

}
