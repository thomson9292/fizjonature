import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as $ from 'jquery';
import { AdminService } from '../../services/admin.service';
import 'rxjs/add/operator/take';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  dataToEdit: Array<any> = [];

  showForm = false;

  @Input() gallery: any;
  @Input() closeEditForm: any;

  @Output()
  galleryInfo: EventEmitter<String> = new EventEmitter<String>();

  @Output()
  openEditForm: EventEmitter<any> = new EventEmitter<any>();

  public galleryId: string;

  // Logowanie
  ifLoggedOn: boolean;

  constructor(private adminService: AdminService) {
  }

  ifLogged() {
    this.ifLoggedOn = this.adminService.ifLogged;
    this.adminService.ifLoggedChange
      .subscribe(value => {
        this.ifLoggedOn = value;
      });
  }

  onToggle() {
    if (this.showForm === false) {
      this.showForm = true;
      this.dataToEdit = [this.showForm, this.gallery];
      this.openEditForm.emit(this.dataToEdit);
    } else {
      this.showForm = false;
    }
  }

  sendId(event) {
    this.galleryId = event;
    this.galleryInfo.emit(this.galleryId);
  }

  ngOnInit() {
    this.ifLogged();
    $(document).ready(function () {
      const $animation_element = $('.gallery');
    });
  }

}
