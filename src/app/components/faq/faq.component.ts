import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import FAQ from '../../constants/faq';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

  faq = FAQ;

  constructor() { }

  ngOnInit() {
    // console.log(this.faq);
    $(document).ready(function () {
      const $changeBox = $('.upper-text');
      $($changeBox).on('click', function () {
        if ($(this).siblings('.hidden-box').is(':visible')) {
          $(this).siblings('.hidden-box').children('.hidden-text').animate({
            left: '-1100px'
          }, function () {
            $(this).parent('.hidden-box').slideUp();
          });
          return false;
        }

        $($changeBox).not($(this)).siblings('.hidden-box').children('.hidden-text').animate({
          left: '-1100px'
        }, function () {
          $(this).parent('.hidden-box').slideUp();
        });

        $(this).siblings('.hidden-box').show('slow', function showNext() {
          $(this).children('.hidden-text').show('fast').animate({
            left: '0'
          });
        });
      });
    });
  }

}
