import { Component, OnInit, Input, Output } from '@angular/core';
import { GalleriesService } from '../../../services/galleries.service';
import * as $ from 'jquery';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { ToasterAppService } from '../../../services/toaster.service';
import { ConfirmationService } from '../../../services/confirmation.service';
import { AdminService } from '../../../services/admin.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  @Input() news: any;
  showMoreText = 'czytaj więcej';
  @Output()
  rotValue = 'rotate(0deg)';
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];

  // Logowanie
  ifLoggedOn: boolean;

  constructor(private galleriesService: GalleriesService, private toastr: ToasterAppService,
    private confirmationService: ConfirmationService, private adminService: AdminService) { }


  ifLogged() {
    this.ifLoggedOn = this.adminService.ifLogged;
    this.adminService.ifLoggedChange
      .subscribe(value => {
        this.ifLoggedOn = value;
      });
  }


  showMore(event) {
    if (this.showMoreText === 'czytaj więcej') {
      this.showMoreText = 'zwiń';
      this.rotValue = 'rotate(180deg)';
    } else if (this.showMoreText === 'zwiń') {
      this.showMoreText = 'czytaj więcej';
      this.rotValue = 'rotate(0deg)';
    }
  }


  deleteComment(arg) {
    this.confirmationService.toggleModalOn('Jestes pewny?');
    this.confirmationService.ifRemoveChange.subscribe(value => {
      if (value) {
        this.confirmationService.modalOn = false;
        this.confirmationService.ifRemove = false;
        this.galleriesService.delSingleData(arg, 'news').toPromise().then((response) => {
          this.toastr.Success('news removed!');
        }, (errResponse) => {
          this.toastr.Error('Something went wrong : ' + errResponse.message, 'Error!');
        });
      }
    });
  }


  ngOnInit() {
    this.showMoreText = 'czytaj więcej';
    this.rotValue = 'rotate(0deg)';

    this.ifLogged();

    // this.galleryOptions = [
    //   { 'image': false, 'height': '150px', 'thumbnailsColumns': 4, 'thumbnailsMargin': 10, 'thumbnailMargin': 10, 'width': '100%', }
    // ];

    this.galleryImages = [];
    if (this.news.photos.length > 0) {
      for (let i = 0; i < this.news.photos.length; i++) {
        this.galleryImages[i] = {
          small: this.news.photos[i].url,
          medium: this.news.photos[i].url,
          big: this.news.photos[i].url,
        };
      }
    }


    this.galleryOptions = [
      { 'image': false, 'height': '200px', 'thumbnailsColumns': 4, 'thumbnailsMargin': 10, 'thumbnailMargin': 10, 'width': '100%' }
    ];

    $(document).ready(function () {
      const $changeBox = $('.news-content');
      const $mainBox = $('.content');

      $('.news-content').each(function () {
        $($mainBox).on('mouseover', function () {
          $(this).css({ 'height': '100%' });
        });
        $($mainBox).on('mouseout', function () {
          $(this).css({ 'height': '0%' });
        });
      });
    });
  }

}
