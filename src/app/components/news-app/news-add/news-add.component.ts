import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { DropzoneComponent, DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { ToasterAppService } from '../../../services/toaster.service';
import { FileUploadService } from '../../../services/file-upload.service';
import { GalleriesService } from '../../../services/galleries.service';
import * as uuid from 'uuid/v4';
import { Inews } from '../../../interfaces/inews';
import { FormGroupDirective } from '@angular/forms';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'news-add',
  templateUrl: './news-add.component.html',
  styleUrls: ['./news-add.component.scss']
})
export class NewsAddComponent implements OnInit {

  public config: DropzoneConfigInterface = {
    clickable: true,
    maxFiles: 10,
    autoReset: 2000,
    errorReset: 2000,
    cancelReset: 2000
  };

  public type = 'component';
  @ViewChild(DropzoneComponent) componentRef?: DropzoneComponent;

  @Output()
  saveGallery: EventEmitter<any> = new EventEmitter<string>();

  news: Inews;
  uploadCounter = 0;
  photosNumber = 0;
  stopUpload = false;
  isUploading = false;
  tempPhoto;
  text = 'This will save you from the exception in case of absence of space in the string';

  constructor(private galleriesService: GalleriesService, private fileUploadService: FileUploadService,
    private toastr: ToasterAppService) { }

  ngOnInit() {
    this.news = this.setEmptyNews();
  }

  clearPopUp() {
    this.isUploading = false;
    this.news = this.setEmptyNews();
    this.componentRef.directiveRef.reset();
  }

  setEmptyNews() {
    return {
      newsId: uuid(),
      title: '',
      thumbUrl: '',
      dateCreated: '',
      description: '',
      descriptionShort: '',
      photos: [
      ]
    };
  }

  setEmptyPhoto() {
    return {
      $key: uuid(),
      file: null,
      name: '',
      url: '',
      progress: 0,
      createdAt: new Date(),
    };
  }

  addPhoto() {
    this.news.photos.push(this.setEmptyPhoto());
  }

  removePhoto(index) {
    this.news.photos.splice(index, 1);
  }

  onUploadError(event) {
    console.log(event);
  }

  onUploadSuccess(event) {
    this.addPhoto();
    this.news.photos[this.news.photos.length - 1] = this.createFileToSend(event[0]);
    console.log(this.news);
    this.photosNumber = this.photosNumber + 1;
  }

  movePhoto(index, up = false) {
    if (up) {
      console.log(index);
      console.log(this.news);
      this.tempPhoto = this.news.photos[index];
      this.news.photos[index] = this.news.photos[index - 1];
      this.news.photos[index - 1] = this.tempPhoto;
      up = null;
    } else {
      this.tempPhoto = this.news.photos[index];
      this.news.photos[index] = this.news.photos[index + 1];
      this.news.photos[index + 1] = this.tempPhoto;
    }
  }

  createFileToSend(file) {
    return {
      $key: uuid(),
      file: file,
      name: file.name,
      url: file.dataURL,
      progress: 0,
      createdAt: new Date(),
    };
  }

  // dodawanie nowej newsa
  async uploadGallery(newsForm: FormGroupDirective) {
    this.isUploading = true;
    for (let i = 0; i < (this.news.photos.length); i++) {

      if (this.stopUpload) {
        return;
      }

      if (this.news.photos.length >= 1) {
        await this.fileUploadService.uploadData(this.news.photos[i], i, 'news-photos/')
          .then((response) => {
            this.news.photos[i].url = response;
            this.news.photos[i].file = null;
            this.uploadCounter = this.uploadCounter + 1;
            this.toastr.Success('zdjecie: ' + this.news.photos[i].name, 'wysłane!');
          }, (errResponse) => {
            this.stopUpload = true;
            this.isUploading = false;
            this.toastr.Error('Something went wrong :' + errResponse, 'Error!');
          });
      }
    }

    if (this.news.photos.length >= 1) {
      console.log('glowny URL ' + this.news.photos[0].url);
      this.news.thumbUrl = this.news.photos[0].url;
    } else {
      this.news.thumbUrl = null;
    }

    if (this.news.description.length > 100) {
      this.news.descriptionShort = this.news.description.slice(0, this.text.indexOf(' ', 30)) + '...';
    } else {
      this.news.descriptionShort = null;
    }

    this.galleriesService.sendData(this.news, 'news')
      .then((response) => {
        this.toastr.Success('News exported!');
        newsForm.resetForm();
        this.clearPopUp();
      }, (errResponse) => {
        this.toastr.Error('Something went wrong :' + errResponse, 'Error!');
      });
  }
}
