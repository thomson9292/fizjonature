import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { AdminService } from '../../services/admin.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {

  @Input() comment: any;
  @Output() delComment: EventEmitter<String> = new EventEmitter<String>();

  // Logowanie
  ifLoggedOn: boolean;

  constructor(private adminService: AdminService) {
  }

  deleteComment(arg) {
    console.log('sendComment ', arg);
    this.delComment.emit(arg);
  }

  ifLogged() {
    this.ifLoggedOn = this.adminService.ifLogged;
    this.adminService.ifLoggedChange
      .subscribe(value => {
        this.ifLoggedOn = value;
      });
  }

  ngOnInit() {
    this.ifLogged();
  }

}
