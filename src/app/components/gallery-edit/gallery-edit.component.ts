import { Component, OnInit, Output, EventEmitter, ViewChild, Input, OnChanges } from '@angular/core';
import * as uuid from 'uuid/v4';
import { GalleriesService } from '../../services/galleries.service';
import { ToasterAppService } from '../../services/toaster.service';
import { FileUploadService } from '../../services/file-upload.service';
import { DropzoneComponent, DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { Itags } from '../../interfaces/itags';
import { TagsService } from '../../services/tags.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gallery-edit',
  templateUrl: './gallery-edit.component.html',
  styleUrls: ['./gallery-edit.component.scss']
})
export class GalleryEditComponent implements OnInit {


  public config: DropzoneConfigInterface = {
    clickable: true,
    maxFiles: 10,
    autoReset: 2000,
    errorReset: 2000,
    cancelReset: 2000
  };

  public type = 'component';
  @ViewChild(DropzoneComponent) componentRef?: DropzoneComponent;

  @Output()
  saveGallery: EventEmitter<any> = new EventEmitter<string>();

  @Input() galleryToEdit: any[] = [];

  gallery;
  uploadCounter = 0;
  photosNumber = 0;
  stopUpload = false;
  isUploading = false;
  filesToRemove: any[] = [];
  filesRemoved = 0;
  tempPhoto;
  uniqueTags: Itags[] = [];
  newUniqueTags: Itags[] = [];
  tagData;

  constructor(public galleriesService: GalleriesService, private fileUploadService: FileUploadService, public toastr: ToasterAppService,
    private tagsService: TagsService) {
  }

  getTags() {
    this.galleriesService.getTags('tags')
      .subscribe(
        data => {
          console.log(data);
          this.uniqueTags = data;
        }
      );
  }

  movePhoto(index, up = false) {
    if (up) {
      console.log(index);
      console.log(this.gallery);
      this.tempPhoto = this.gallery.photos[index];
      this.gallery.photos[index] = this.gallery.photos[index - 1];
      this.gallery.photos[index - 1] = this.tempPhoto;
      up = null;
    } else {
      this.tempPhoto = this.gallery.photos[index];
      this.gallery.photos[index] = this.gallery.photos[index + 1];
      this.gallery.photos[index + 1] = this.tempPhoto;
    }
  }

  clearPopUp() {
    this.isUploading = false;
    this.gallery = this.setEmptyGallery();
    this.componentRef.directiveRef.reset();
    this.gallery.photos = [];
  }

  setEmptyGallery() {
    return {
      galleryId: this.gallery.galleryId,
      title: '',
      thumbUrl: '',
      dateCreated: '',
      description: '',
      tags: [],
      photos: [
        this.setEmptyPhoto()
      ]
    };
  }

  setEmptyPhoto() {
    return {
      $key: uuid(),
      file: null,
      name: '',
      url: '',
      progress: 0,
      createdAt: new Date(),
    };
  }

  restoreData() {
    this.filesRemoved = 0;
    this.filesToRemove = [];
    this.gallery = this.galleriesService.getSpecificGallery(this.gallery.galleryId, 'galleries').subscribe(
      data => {
        this.gallery = data;
        this.getTags();
        console.log('Gallery after restore ', this.gallery);
      }
    );
  }

  addPhoto() {
    this.gallery.photos.push(this.setEmptyPhoto());
  }

  removePhoto(index, newPhoto = false) {
    if (this.gallery.photos[index].url) {
      this.filesToRemove[this.filesRemoved] = this.gallery.photos[index].$key;
      this.filesRemoved = this.filesRemoved + 1;
      console.log('Files to remove ', this.filesToRemove);
      console.log(this.gallery.photos.length);
    }
    this.gallery.photos.splice(index, 1);
  }

  onUploadError(event) {
    console.log(event);
  }

  onUploadSuccess(event) {
    this.addPhoto();
    this.gallery.photos[this.gallery.photos.length - 1] = this.createFileToSend(event[0]);
    this.photosNumber = this.photosNumber + 1;
  }

  createFileToSend(file) {
    return {
      $key: uuid(),
      file: file,
      name: file.name,
      url: file.dataURL,
      progress: 0,
      createdAt: new Date(),
    };
  }

  // dodawanie nowej galerii
  async uploadGallery() {
    this.tagData = this.tagsService.setNewListOfTags(this.gallery, this.uniqueTags);
    this.gallery = this.tagData[0];
    this.newUniqueTags = this.tagData[1];

    this.isUploading = true;
    if (this.filesToRemove.length > 0) {
      for (let i = 0; i < this.filesToRemove.length; i++) {
        this.fileUploadService.removeData(this.filesToRemove[i], 'blog-photos/');
      }
    }

    for (let i = 0; i < (this.gallery.photos.length); i++) {
      if (this.stopUpload) {
        return;
      }
      if (this.gallery.photos[i].file) {
        console.log(this.gallery.photos[i]);
        await this.fileUploadService.uploadData(this.gallery.photos[i], i, 'blog-photos/')
          .then((response) => {
            this.gallery.photos[i].url = response;
            this.gallery.photos[i].file = null;
            this.uploadCounter = this.uploadCounter + 1;
            this.toastr.Success('Photo: ' + this.gallery.photos[i].name, 'Gallery exported!');
          }, (errResponse) => {
            this.stopUpload = true;
            this.isUploading = false;
            console.log(errResponse);
            this.toastr.Error('Something went wrong :' + errResponse, 'Error!');
          });
      }
    }

    console.log('glowny URL ' + this.gallery.photos[0].url);
    this.gallery.thumbUrl = this.gallery.photos[0].url;

    console.log('DLUGOSC TAGOW ' + this.newUniqueTags.length);
    for (let i = 0; i < (this.newUniqueTags.length); i++) {
      console.log(this.newUniqueTags[i]);
      // this.galleriesService.updateData(this.newUniqueTags[i], 'tags', 'data.tagId')
      this.galleriesService.sendData(this.newUniqueTags[i], 'tags')
        .then((response) => {
          this.toastr.Success('New tag added!');
        }, (errResponse) => {
          this.toastr.Error('Adding tags went wrong :' + errResponse, 'Error!');
        });
    }

    this.galleriesService.updateData(this.gallery, 'galleries', this.gallery.galleryId)
      .then((response) => {
        this.toastr.Success(response, 'Data updated!');
        this.filesToRemove = [];
        this.filesRemoved = 0;
      }, (errResponse) => {
        this.toastr.Error('Something went wrong :' + errResponse, 'Error!');
      });
  }

  ngOnInit() {
    this.gallery = this.galleryToEdit;
    console.log(this.gallery);
    this.restoreData();
  }
}
