import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { NgxGalleryOptions, NgxGalleryImage } from 'ngx-gallery';
import certs from '../../../constants/certifications';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.scss']
})
export class PersonalDetailsComponent implements OnInit {

  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];

  constructor() {
  }

  showElements() {
      $(document).scroll(function () {
        if ($('.description-box1').length === 0) {
          return;
        }
        const window_height = $(window).height();
        const window_top_position = $(window).scrollTop();
        const window_bottom_position = (window_top_position + window_height);

        if (window_bottom_position > ($('.photo-box1').position().top + $('.photo-box1').height() / 2)) {
          $('.description-box1').animate({
            left: '0'
          }, 1000);
          $('.photo-box1').animate({
            left: '0'
          }, 1000);
        }

        if (window_bottom_position > ($('.photo-box2').position().top + $('.photo-box2').height() / 4)) {
          $('.description-box-title').animate({
            top: '0'
          }, 1000);
          $('.description-box2').animate({
            top: '0'
          }, 1000);
          $('.photo-box2').animate({
            left: '0'
          }, 1000);
        }

        if (window_bottom_position > ($('.photo-box3').position().top + $('.photo-box3').height() / 3)) {
          $('.description-box3').animate({
            left: '0'
          }, 1000);
          $('.photo-box3').animate({
            left: '0'
          }, 1000);
        }

        if (window_bottom_position > ($('.description-box4').position().top)) {
          $('.description-box4').css({
            transform: 'scale(1)'
          });
          $('.photo-box4').each(function (i) {
            const delay = (i) * 300;
            setTimeout(function (div) {
              div.animate({
                top: '0'
              }, 1000);
            }, delay, $(this));
          });
        }

      });

  }


  ngOnInit() {
    this.showElements();

    this.galleryOptions = [
      { 'image': false, 'height': '150px', 'thumbnailsColumns': 5, 'thumbnailsMargin': 10, 'thumbnailMargin': 10, 'width': '100%' }
    ];
    this.galleryImages = certs;
  }

}
