import { Component, OnInit } from '@angular/core';
import { GalleriesService } from '../../services/galleries.service';
import { ToasterAppService } from '../../services/toaster.service';
import { Itags } from '../../interfaces/itags';
import 'rxjs/add/operator/take';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'tags-color',
  templateUrl: './tags-color.component.html',
  styleUrls: ['./tags-color.component.scss']
})
export class TagsColorComponent implements OnInit {

  uniqueTags: Itags[] = [];

  constructor(private galleriesService: GalleriesService, public toastr: ToasterAppService) { }

  getTags() {
    this.galleriesService.getTags('tags').take(1)
      .subscribe(
        data => {
          console.log(data);
          this.uniqueTags = data;
        }
      );
  }

  updateColors() {
    console.log(this.uniqueTags);
    this.galleriesService.updateColors('galleries', this.uniqueTags);
    for (let i = 0; i < (this.uniqueTags.length); i++) {
      this.galleriesService.updateData(this.uniqueTags[i], 'tags', this.uniqueTags[i].tagId)
        .then((response) => {
          this.toastr.Success('tag color updated!');
        }, (errResponse) => {
          this.toastr.Error('can not update tag :' + errResponse, 'Error!');
        });
    }
  }

  ngOnInit() {
    this.getTags();
  }

}
