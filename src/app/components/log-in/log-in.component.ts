import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import * as $ from 'jquery';
import { AdminService } from '../../services/admin.service';
import { ToasterAppService } from '../../services/toaster.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {

  email;
  pass;
  auth;
  user;
  loggedUserFlag;

  constructor(private adminService: AdminService, private toastr: ToasterAppService) { }

  logIn() {
    this.loggedUserFlag = true;
    this.auth = firebase.auth()
      .signInWithEmailAndPassword(this.email, this.pass)
      .catch(m => {
        console.log(m.message);
        this.toastr.Error(m.message);
      });

    firebase.auth().onAuthStateChanged(firebaseUser => {
      if (firebaseUser && this.loggedUserFlag) {
        console.log(firebaseUser);
        this.adminService.toggleIfLogged(true);
        this.toastr.Success('Logged successfully');
        this.loggedUserFlag = false;
      }
    });
  }

  signUp() {
    console.log(this.pass);
    if (this.pass === undefined && this.email === undefined) {
      this.toastr.Error('provide user data');
    }
    this.auth = firebase.auth()
      .createUserWithEmailAndPassword(this.email, this.pass)
      .catch(m => {
        console.log(m.message);
        this.toastr.Error(m.message);
      });

  }

  logOut() {
    firebase.auth().signOut();
  }

  checkIfLoggedIn() {
    this.adminService.ifLoggedChange
      .subscribe(value => {
        this.user = value;
      });
  }

  ngOnInit() {
    this.checkIfLoggedIn();
    $(document).ready(function () {
      const $msgBtn = ('#mail');
      const $commentsBtn = ('#comments');
      const $tagBtn = ('#tags');
      const $msgbox = ('#inner-mail-box');
      const $incommentsbox = ('#inner-comments-box');
      const $tagsBox = ('#inner-tags-box');

      $($commentsBtn).on('click', function () {
        $(this).css('background-color', '#85B24B').children('#comment-text').css('color', '#ffffff');
        $(this).siblings($msgBtn).css('background-color', 'transparent').children('#mail-text').css('color', '#535353');
        $(this).siblings($tagBtn).css('background-color', 'transparent').children('#tags-text').css('color', '#535353');
        $($msgbox).parent('#mail-box').hide('fast');
        $($tagsBox).parent('#tags-box').hide('fast');
        $($incommentsbox).parent('#comments-box').fadeIn('fast');
      });

      $($msgBtn).on('click', function () {
        $(this).css('background-color', '#85B24B').children('#mail-text').css('color', '#ffffff');
        $(this).siblings($commentsBtn).css('background-color', 'transparent').children('#comment-text').css('color', '#535353');
        $(this).siblings($tagBtn).css('background-color', 'transparent').children('#tags-text').css('color', '#535353');
        $($incommentsbox).parent('#comments-box').hide('fast');
        $($tagsBox).parent('#tags-box').hide('fast');
        $($msgbox).parent('#mail-box').fadeIn('fast');
      });

      $($tagBtn).on('click', function () {
        $(this).css('background-color', '#85B24B').children('#tags-text').css('color', '#ffffff');
        $(this).siblings($commentsBtn).css('background-color', 'transparent').children('#comment-text').css('color', '#535353');
        $(this).siblings($msgBtn).css('background-color', 'transparent').children('#mail-text').css('color', '#535353');
        $($incommentsbox).parent('#comments-box').hide('fast');
        $($msgbox).parent('#mail-box').hide('fast');
        $($tagsBox).parent('#tags-box').fadeIn('fast');
      });
    });
  }
}
