import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IGallery } from '../../interfaces/igallery';
import { CommentsService } from '../../services/comments.service';
import { Icomment } from '../../interfaces/icomment';
import { ToasterAppService } from '../../services/toaster.service';
import { GalleriesService } from '../../services/galleries.service';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import * as $ from 'jquery';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gallery-details',
  templateUrl: './gallery-details.component.html',
  styleUrls: ['./gallery-details.component.scss']
})
export class GalleryDetailsComponent implements OnInit {

  galleryId: string;
  gallery: IGallery;
  mainPhoto: string;
  galleries: any = [];

  // comment: Icomment[];
  comments: Icomment[] = [];

  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];

  constructor(private route: ActivatedRoute, private commentsService: CommentsService, private galleriesService: GalleriesService,
    private toastr: ToasterAppService) {
  }

  // Pobranie komentarzy do galerii
  getComments() {
    this.galleriesService.getComments('comments', this.galleryId)
      .subscribe(
        data => {
          // console.log(data);
          this.comments = data
            .filter(item =>
              (item.galleryId === this.galleryId));
        }
      );
  }

  // Zmiana dużego zdjęcia po kliknięciu
  changePhoto(event) {
    this.mainPhoto = event.target.src;
    this.gallery.thumbUrl = this.mainPhoto;
  }

  // (eventEmmiter) Pobranie od childa danych o nowo dodanym komentarzu
  updateComments(event) {
    this.comments.push(event);
  }

  // Usuniecie komentarza
  deleteComment(event) {
    console.log(event);
    this.galleriesService.delSingleData(event, 'comments').toPromise().then((response) => {
      this.toastr.Success('Gallery removed!');
    }, (errResponse) => {
      this.toastr.Error('Something went wrong : ' + errResponse.message, 'Error!');
    });
  }

  // UWAGA, lepiej uzyc metody getSpecificGallery()
  ngOnInit() {
    this.galleriesService.getGalleries('galleries')
      .subscribe(
        data => {
          this.galleries = data;
          this.galleryId = this.route.snapshot.paramMap.get('galleryId');
          this.gallery = this.galleries
            .find(item =>
              item.galleryId === Number(this.galleryId) || item.galleryId === this.galleryId);

          this.galleryImages = [];
          if (this.gallery.photos.length > 0) {
            for (let i = 0; i < this.gallery.photos.length; i++) {
              this.galleryImages[i] = {
                small: this.gallery.photos[i].url,
                medium: this.gallery.photos[i].url,
                big: this.gallery.photos[i].url,
                // description: ' '
              };
            }
          }
        }
      );
    this.getComments();

    this.galleryOptions = [
      {
        width: '100%',
        height: '600px',
        thumbnailsColumns: 4,
        imageAnimation: NgxGalleryAnimation.Slide,
        imageDescription: true,
      },
      // max-width 800
      {
        breakpoint: 800,
        width: '100%',
        height: '600px',
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
      },
      // max-width 400
      {
        breakpoint: 400,
        preview: false
      }
    ];
    $(document).ready(function () {
        $([document.documentElement, document.body]).animate({
          scrollTop: $('.slider-img').offset().top + $('#box').height()
        }, 1000);
    });
  }
}
