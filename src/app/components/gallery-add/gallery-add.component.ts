import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { IGallery } from '../../interfaces/igallery';
import * as uuid from 'uuid/v4';
import { GalleriesService } from '../../services/galleries.service';
import { ToasterAppService } from '../../services/toaster.service';
import { FileUploadService } from '../../services/file-upload.service';
import { DropzoneComponent, DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { Itags } from '../../interfaces/itags';
import { TagsService } from '../../services/tags.service';
import { FormBuilder, FormGroupDirective } from '@angular/forms';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gallery-add',
  templateUrl: './gallery-add.component.html',
  styleUrls: ['./gallery-add.component.scss']
})
export class GalleryAddComponent implements OnInit {

  public config: DropzoneConfigInterface = {
    clickable: true,
    maxFiles: 10,
    autoReset: 2000,
    errorReset: 2000,
    cancelReset: 2000
  };

  public type = 'component';
  @ViewChild(DropzoneComponent) componentRef?: DropzoneComponent;

  @Output()
  saveGallery: EventEmitter<any> = new EventEmitter<string>();

  gallery: IGallery;
  stopUpload = false;
  isUploading = false;
  tempPhoto;
  uniqueTags: Itags[] = [];
  newUniqueTags: Itags[] = [];
  tagData;
  resetForm: HTMLElement;

  constructor(private galleriesService: GalleriesService, private fileUploadService: FileUploadService,
    public toastr: ToasterAppService, private tagsService: TagsService, private formBuilder: FormBuilder) {
  }

  getTags() {
    this.galleriesService.getTags('tags')
      .subscribe(
        data => {
          console.log(data);
          this.uniqueTags = data;
        }
      );
  }

  clearPopUp() {
    this.isUploading = false;
    this.gallery = this.setEmptyGallery();
    this.componentRef.directiveRef.reset();
  }

  setEmptyGallery() {
    return {
      galleryId: uuid(),
      title: '',
      thumbUrl: '',
      dateCreated: '',
      description: '',
      tags: [],
      photos: [
        // this.setEmptyPhoto()
      ]
    };
  }

  setEmptyForm() {
    this.isUploading = false;
    this.gallery = this.setEmptyGallery();
    this.componentRef.directiveRef.reset();
    this.clearPopUp();
    this.newUniqueTags = null;
  }

  setEmptyPhoto() {
    return {
      $key: uuid(),
      file: null,
      name: '',
      url: '',
      progress: 0,
      createdAt: new Date(),
    };
  }


  addPhoto() {
    this.gallery.photos.push(this.setEmptyPhoto());
  }

  removePhoto(index) {
    this.gallery.photos.splice(index, 1);
  }

  onUploadError(event) {
    console.log(event);
  }

  onUploadSuccess(event) {
    this.addPhoto();
    this.gallery.photos[this.gallery.photos.length - 1] = this.createFileToSend(event[0]);
    console.log(this.gallery);
  }

  movePhoto(index, up = false) {
    if (up) {
      console.log(index);
      console.log(this.gallery);
      this.tempPhoto = this.gallery.photos[index];
      this.gallery.photos[index] = this.gallery.photos[index - 1];
      this.gallery.photos[index - 1] = this.tempPhoto;
      up = null;
    } else {
      this.tempPhoto = this.gallery.photos[index];
      this.gallery.photos[index] = this.gallery.photos[index + 1];
      this.gallery.photos[index + 1] = this.tempPhoto;
    }
  }

  createFileToSend(file) {
    return {
      $key: uuid(),
      file: file,
      name: file.name,
      url: file.dataURL,
      progress: 0,
      createdAt: new Date(),
    };
  }

  // setForm() {
  //   this.galleryForm = this.formBuilder.group({
  //     title: ['', Validators.required],
  //     dateCreated: ['', Validators.required],
  //     tags: ['', Validators.required],
  //     description: ['', Validators.required],
  //   });
  // }
  // get f() { return this.galleryForm.controls; }

  // dodawanie nowej galerii
  async uploadGallery(galleryForm: FormGroupDirective) {
    this.tagData = this.tagsService.setNewListOfTags(this.gallery, this.uniqueTags);
    this.gallery = this.tagData[0];
    this.newUniqueTags = this.tagData[1];

    this.isUploading = true;
    for (let i = 0; i < (this.gallery.photos.length); i++) {
      if (this.stopUpload) {
        return;
      }
      await this.fileUploadService.uploadData(this.gallery.photos[i], i, 'blog-photos/')
        .then((response) => {
          this.gallery.photos[i].url = response;
          this.gallery.photos[i].file = null;
          this.toastr.Success('photo: ' + this.gallery.photos[i].name, 'added!');
        }, (errResponse) => {
          this.stopUpload = true;
          this.isUploading = false;
          this.toastr.Error('Sending photos went wrong :' + errResponse, 'Error!');
        });
    }
    console.log('glowny URL ' + this.gallery.photos[0].url);
    this.gallery.thumbUrl = this.gallery.photos[0].url;

    console.log('DLUGOSC TAGOW ' + this.newUniqueTags.length);
    for (let i = 0; i < (this.newUniqueTags.length); i++) {
      this.galleriesService.sendData(this.newUniqueTags[i], 'tags')
        .then((response) => {
          this.toastr.Success('New tag added!');
        }, (errResponse) => {
          this.toastr.Error('Adding tags went wrong :' + errResponse, 'Error!');
        });
    }

    this.galleriesService.sendData(this.gallery, 'galleries')
      .then((response) => {
        this.toastr.Success('Blog exported!');
        galleryForm.resetForm();
        this.clearPopUp();
      }, (errResponse) => {
        this.toastr.Error('Blog exporting went wrong :' + errResponse, 'Error!');
      });
  }


  ngOnInit() {
    this.gallery = this.setEmptyGallery();
    this.getTags();
  }


}
