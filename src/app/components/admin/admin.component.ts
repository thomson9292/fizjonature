import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { GalleriesService } from '../../services/galleries.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  open: boolean;

  // formularz dodawania galerii
  newsForm = false;
  galleryForm = false;
  formButton = 'AddGallery';

  constructor(private galleriesService: GalleriesService) {
  }

  onToggle(event, type?) {
    if (event.srcElement.innerText === 'Add gallery') {
      this.galleryForm = true;
      $('body').css('overflow', 'hidden');
      $('#navigation').css('visibility', 'hidden');
      $('html,body').animate({ scrollTop: 0 }, 'slow');
    } else if (event.srcElement.innerText === 'Add news') {
      this.newsForm = true;
      $('body').css('overflow', 'hidden');
      $('#navigation').css('visibility', 'hidden');
      $('html,body').animate({ scrollTop: 0 }, 'slow');
    } else if (event.srcElement.id === 'cancel') {
      this.galleryForm = false;
      this.newsForm = false;
      $('body').css('overflow', 'auto');
      $('#navigation').css('visibility', 'visible');
    }
  }


  ngOnInit() { }

}
