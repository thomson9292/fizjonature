import { Component, OnInit, ViewChild } from '@angular/core';
import { Imail } from '../../interfaces/imail';
import { GalleriesService } from '../../services/galleries.service';
import { ToasterAppService } from '../../services/toaster.service';
import * as uuid from 'uuid/v4';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  lat = 53.439365;
  lng = 14.476882;
  mail: Imail;

  constructor(private galleriesService: GalleriesService, private toastr: ToasterAppService) { }

  setEmptyMail() {
    return {
      mailId: uuid(),
      dateCreated: new Date(),
      name: '',
      surname: '',
      userEmail: '',
      subject: '',
      text: '',
    };
  }

  ngOnInit() {
    this.mail = this.setEmptyMail();
  }

  sendMail() {
    this.galleriesService.sendData(this.mail, 'mails')
      .then((response) => {
        this.toastr.Success('Wysłano wiadomość!');
        this.mail = this.setEmptyMail();
      }, (errResponse) => {
        this.toastr.Error('Błąd :' + errResponse, 'Error!');
        this.mail = this.setEmptyMail();
      });
  }

}
