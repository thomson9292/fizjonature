import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from '../../services/confirmation.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {

  modal;
  text;

  constructor(private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.confirmationService.modalOnChange.subscribe(value => {
      this.modal = value[0];
      this.text = value[1];
      console.log(value);
    });
  }

  removal(ifRemove) {
    this.confirmationService.toggleModalOn('');
    if (ifRemove === 1) {
      this.confirmationService.toggleIfRemove();
    }
  }

}
