import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import * as $ from 'jquery';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'offer-details',
  templateUrl: './offer-details.component.html',
  styleUrls: ['./offer-details.component.scss']
})
export class OfferDetailsComponent implements OnInit {

  @Input() offer;
  lastOpened: number;


  constructor() { }

  showOffer(offer, lastOpened?) {
    if (this.lastOpened) {
      $('.inner' + lastOpened).animate({
        right: '-1900px'
      }, function () {
        $(this).parent('.offer' + lastOpened).slideUp('fast', function () {
          $('.offer' + offer).show('fast', function showNext() {
            $(this).children('.inner' + offer).show('fast', function showNext2() {
              $(this).animate({
                right: '0'
              }, 1000);
            });
            $([document.documentElement, document.body]).animate({
              scrollTop: $('.offer' + offer).offset().top
            }, 1000);
          });
        });
      });
    } else {
      $('.offer' + offer).show('fast', function showNext() {
        $(this).children('.inner' + offer).show('fast', function showNext2() {
          $(this).animate({
            right: '0'
          }, 1000);
        });
        $([document.documentElement, document.body]).animate({
          scrollTop: $('.offer' + offer).offset().top
        }, 1000);
      });
      $('#height-box').show('fast');
    }

    this.lastOpened = offer;
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges(changes: SimpleChanges): void {
    switch (this.offer) {
      case 1: {
        this.showOffer(this.offer, this.lastOpened);
        break;
      }
      case 2: {
        this.showOffer(this.offer, this.lastOpened);
        break;
      }
      case 3: {
        this.showOffer(this.offer, this.lastOpened);
        break;
      }
      case 4: {
        this.showOffer(this.offer, this.lastOpened);
        break;
      }
      case 5: {
        this.showOffer(this.offer, this.lastOpened);
        break;
      }
      case 6: {
        this.showOffer(this.offer, this.lastOpened);
        break;
      }
      default: {
        break;
      }
    }
  }

  ngOnInit() {
  }

}
