import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.scss']
})
export class OfferComponent implements OnInit {

  offerToDisplay: number;

  constructor() { }

  openOffer(event) {
    this.offerToDisplay = event;
  }

  ngOnInit() {

    $('.element').each(function () {
      $(this).mouseover(function () {
        $(this).addClass('active');
        $('.stage').children('.element').not('.active').addClass('inactive');
        $('.stage2').children('.element2').addClass('other-pen');
      });
      $(this).mouseleave(function () {
        $(this).removeClass('active');
        $('.stage').children('.element').not('.active').removeClass('inactive');
        $('.stage2').children('.element2').removeClass('other-pen');
      });
    });
    $('.element2').each(function () {
      $(this).mouseover(function () {
        $(this).addClass('active');
        $('.stage2').children('.element2').not('.active').addClass('inactive');
        $('.stage').children('.element').addClass('other-pen');
      });
      $(this).mouseleave(function () {
        $(this).removeClass('active');
        $('.stage2').children('.element2').not('.active').removeClass('inactive');
        $('.stage').children('.element').removeClass('other-pen');
      });
    });
  }

}
