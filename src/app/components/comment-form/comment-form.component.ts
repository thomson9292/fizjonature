import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import * as uuid from 'uuid/v4';
import { Icomment } from '../../interfaces/icomment';
import { ActivatedRoute } from '@angular/router';
// import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToasterAppService } from '../../services/toaster.service';
import { CommentsService } from '../../services/comments.service';
import { GalleriesService } from '../../services/galleries.service';
import { NgForm, FormGroupDirective } from '@angular/forms';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.scss']
})
export class CommentFormComponent implements OnInit {


  @Output() pushComment: EventEmitter<String> = new EventEmitter<String>();

  comment: Icomment;
  comments: Icomment[] = [];
  galleryId: string;

  constructor(private route: ActivatedRoute, private galleriesService: GalleriesService, public toastr: ToasterAppService) { }

  ngOnInit() {
    this.galleryId = this.route.snapshot.paramMap.get('galleryId');
    this.comment = this.setEmptyComment();

    // this.http.get('http://project.usagi.pl/comment/byGallery/' + this.galleryId, this.httpOptions).toPromise().then((response) => {
    //   this.comments = <any>response;
    // });
  }


  sendComment(arg) {
    console.log(arg);
    this.pushComment.emit(arg);
  }


  onDelete(commentId) {
    // const index = this.comments.findIndex(item => item.commentId ===
    //   commentId);
    // this.http.post('http://project.usagi.pl/comment/delete/' +
    //   commentId, {}, this.httpOptions).toPromise().then(() => {
    //     this.comments.splice(index, 1);
    //     this.toastr.Success('Comment removed');
    //   }, (errResponse) => {
    //     this.toastr.Error('Something went wrong!');
    //   });
  }


  onSubmit(commentForm: FormGroupDirective) {
    this.galleriesService.sendData(this.comment, 'comments')
      .then((response) => {
        this.toastr.Success('Gallery exported!');
        this.comment = this.setEmptyComment();
        commentForm.resetForm();
        // this.form.reset();
      }, (errResponse) => {
        this.toastr.Error('Something went wrong :' + errResponse, 'Error!');
      });
  }


  private setEmptyComment() {
    return {
      galleryId: this.galleryId,
      commentId: uuid(),
      firstName: '',
      lastName: '',
      email: '',
      message: '',
      dateCreated: new Date(),
    };
  }
}
