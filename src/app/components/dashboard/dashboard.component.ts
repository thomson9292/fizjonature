import { Component, OnInit } from '@angular/core';
import { GalleriesService } from '../../services/galleries.service';
import { Inews } from '../../interfaces/inews';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  // @ViewChild('pagination') child: PaginationComponent;
  lastGalleries: any;
  news: Inews[];
  newsStatic = [];

  // pagination
  dataToChild;
  itemsPerPage = 3;
  start: number;
  end: number;

  constructor(public galleriesService: GalleriesService) { }

  // compare(a, b) {
  //   if (a.dateCreated < b.dateCreated) {
  //     return -1;
  //   }
  //   if (a.dateCreated > b.dateCreated) {
  //     return 1;
  //   }
  //   return 0;
  // }

  getPageData(event) {
    this.start = event.start;
    this.end = event.end;
  }

  getNews() {
    this.galleriesService.getNews('news')
      .subscribe(
        data => {
          this.news = data;
          this.dataToChild = { items: this.news, itemsPerPage: this.itemsPerPage };
        }
      );
  }

  getLastGalleries() {
    this.galleriesService.getGalleries('galleries')
      .subscribe(
        data => {
          this.lastGalleries = data;
        }
      );
  }

  getGalleries() {
    this.galleriesService.getGalleries('galleries')
      .subscribe(
        data => {
          this.lastGalleries = data;
        }
      );
  }

  ngOnInit() {
    this.getNews();
    this.getGalleries();
  }

}
