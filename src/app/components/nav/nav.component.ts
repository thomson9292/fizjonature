import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  constructor() { }

  mobileMenu() {
    $('.dropbtn-nav').on('click', function () {
      if ($('.dropdown-content-nav').css('top') === '-700px') {
        $('.dropbtn-nav').children('i').css('color', '#85B24B');
        $('.dropdown-content-nav').animate({
          top: '100px'
        });
      }
    });

    $(document).click(function () {
      if ($('.dropdown-content-nav').css('top') === '100px') {
        $('.dropbtn-nav').children('i').css('color', '#383838');
        $('.dropdown-content-nav').animate({
          top: '-700px'
        });
      }
    });
  }

  ngOnInit() {
    $('.under').on('click', function () {
      const el = $('#box').position().top + $('#box').height();
      $('html,body').animate({ scrollTop: el }, 'slow');
    });

    this.mobileMenu();
  }

}
