import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  // stronnicowanie
  limit: number;
  currentPage: number;
  start: number;
  end: number;
  numberOfPages: any;
  showPages: any;

  @Input() itemsList: any;
  @Output()
  pageData: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  sendPageInfo() {
    this.pageData.emit({ start: this.start, end: this.end });
  }


  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges(changes: SimpleChanges): void {
    // console.log(changes);
    this.setNumberOfPages();
    this.setCurrentPage();
    this.sendPageInfo();
  }

  // ustawienie aktualnej strony
  setCurrentPage(page = 0): any {
    this.currentPage = parseInt(localStorage.getItem('galleryPage'), 10) || 0;
    this.limit = this.itemsList.itemsPerPage;
    this.currentPage = page;
    this.start = this.currentPage * this.limit;
    this.end = this.start + this.limit;
    localStorage.setItem('galleryPage', this.currentPage.toString());
    this.sendPageInfo();
  }

  // ustalenie liczy stron dla galerii
  setNumberOfPages(): any {
    this.numberOfPages = Array(Math.ceil(this.itemsList.items.length / this.itemsList.itemsPerPage)).fill(1);
    for (let i = 0; i < this.numberOfPages.length; i++) {
      this.numberOfPages[i] += i - 1;
    }
  }

  ngOnInit() {
    this.setNumberOfPages();
    this.setCurrentPage();
    this.sendPageInfo();
  }

}
