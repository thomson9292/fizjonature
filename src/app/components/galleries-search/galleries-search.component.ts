import { Component, OnInit, Output, EventEmitter, Input, SimpleChanges, ViewChild, ElementRef } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'galleries-search',
  templateUrl: './galleries-search.component.html',
  styleUrls: ['./galleries-search.component.scss']
})
export class GalleriesSearchComponent implements OnInit {

  @Input() galleries: any[] = [];
  @Input() tag: any;

  @ViewChild('searchInput') searchInput: ElementRef;

  @Output()
  searchValue: EventEmitter<String> = new EventEmitter<String>();

  public value: string;

  constructor() {
  }

  onChange() {
    this.searchValue.emit(this.value);
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.tag) {
      this.searchInput.nativeElement.value = this.tag;
      this.value = this.tag;
      this.onChange();
    }
  }

  ngOnInit() {
  }

}
