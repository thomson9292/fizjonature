import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'main-photo-slider',
  templateUrl: './main-photo-slider.component.html',
  styleUrls: ['./main-photo-slider.component.scss']
})
export class MainPhotoSliderComponent implements OnInit {

  constructor() { }

  showUpBtn() {
    $(window).on('scroll', function () {
      const window_height = $(window).height();
      const window_top_position = $(window).scrollTop();
      const window_bottom_position = (window_top_position + window_height);
      if (window_bottom_position > $('.footer').position().top) {
        $('#go-up').css('height', '50px');
        $('#go-up').children('.icon').fadeIn('slow');
      } else {
        $('#go-up').children('.icon').fadeOut('slow');
        $('#go-up').css('height', '0px');
      }
    });
  }

  goUp() {
    $('html,body').animate({ scrollTop: 0 }, 'slow');
    return false;
  }

  visitBox() {
    $('#visit-outer').fadeIn(3000);
    $('#photo-cover').animate({
      marginLeft: '0'
    }, 2000);
    $('#visit-outer').children('.visit-inner').animate({
      top: '0'
    }, 2000, function () {
      $(this).siblings('.btn-visit').animate({
        left: '0'
      }, 1000);
    });
  }


  ngOnInit() {
    this.visitBox();
    this.showUpBtn();
  }
}
