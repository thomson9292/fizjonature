// const GALLERY: Gallery[] =
export default [
  {
    'galleryId': '1',
    'title': 'Alaska',
    'dateCreated': '2014-11-15',
    'thumbUrl': './assets/img/biegun-2014/1.jpg',
    'description': 'Niezapomniane wakacje na Alasce.',
    'tags': [
      { 'display': 'wakacje', 'value': 'wakacje', 'color': '#85B24B', 'tagId': '1' },
      { 'display': 'odpoczynek', 'value': 'odpoczynek', 'color': '#85B24B', 'tagId': '2' },
      { 'display': 'wodospad', 'value': 'wodospad', 'color': '#85B24B', 'tagId': '3' },
      { 'display': 'auto', 'value': 'auto', 'color': '#85B24B', 'tagId': '4' },
    ],
    'photos': [{
      'photoId': '1',
      'thumbUrl': './assets/img/biegun-2014/1.jpg',
      'url': './assets/img/biegun-2014/1.jpg',
      'name': 'biegun1',
      'progress': '100'
    },
    {
      'photoId': '2',
      'thumbUrl': './assets/img/biegun-2014/2.jpg',
      'url': './assets/img/biegun-2014/2.jpg',
      'name': 'biegun2',
      'progress': '100'
    },
    {
      'photoId': '3',
      'thumbUrl': './assets/img/biegun-2014/3.jpg',
      'url': './assets/img/biegun-2014/3.jpg',
      'name': 'biegun3',
      'progress': '100'
    },
    {
      'photoId': '4',
      'thumbUrl': './assets/img/biegun-2014/4.jpg',
      'url': './assets/img/biegun-2014/4.jpg',
      'name': 'biegun4',
      'progress': '100'
    }]
  },
  {
    'galleryId': '2',
    'title': 'USA',
    'dateCreated': '2014-02-15',
    'thumbUrl': './assets/img/USA-2014/1.jpg',
    'description': 'Odwiedziny u wuja w USA.',
    'tags': [
      // 'USA',
      // 'kalifornia',
      // 'hamburger'
      { 'display': 'wakacje', 'value': 'wakacje', 'color': '#85B24B', 'tagId': '5' },
      { 'display': 'odpoczynek', 'value': 'odpoczynek', 'color': '#85B24B', 'tagId': '6' },
      { 'display': 'wodospad', 'value': 'wodospad', 'color': '#85B24B', 'tagId': '7' },
      { 'display': 'auto', 'value': 'auto', 'color': '#85B24B', 'tagId': '8' },
    ],
    'photos': [{
      'photoId': '1',
      'thumbUrl': './assets/img/USA-2014/1.jpg',
      'url': './assets/img/USA-2014/1.jpg',
      'name': 'usa1',
      'progress': '100'
    },
    {
      'photoId': '2',
      'thumbUrl': './assets/img/USA-2014/2.jpg',
      'url': './assets/img/USA-2014/2.jpg',
      'name': 'usa2',
      'progress': '100'
    },
    {
      'photoId': '3',
      'thumbUrl': './assets/img/USA-2014/3.jpg',
      'url': './assets/img/USA-2014/3.jpg',
      'name': 'usa3',
      'progress': '100'
    },
    {
      'photoId': '4',
      'thumbUrl': './assets/img/USA-2014/4.jpg',
      'url': './assets/img/USA-2014/4.jpg',
      'name': 'usa4',
      'progress': '100'
    }]
  },
  {
    'galleryId': '3',
    'title': 'Szwajcaria',
    'dateCreated': '2014-05-15',
    'thumbUrl': './assets/img/switzerland-2014/1.jpg',
    'description': 'Piękne wspomnienia z moich wakacji w Szwecji.',
    'tags': [
      { 'display': 'wakacje', 'value': 'wakacje', 'color': '#85B24B', 'tagId': '9' },
      { 'display': 'odpoczynek', 'value': 'odpoczynek', 'color': '#85B24B', 'tagId': '10' },
      { 'display': 'wodospad', 'value': 'wodospad', 'color': '#85B24B', 'tagId': '11' },
      { 'display': 'auto', 'value': 'auto', 'color': '#85B24B', 'tagId': '12' },
    ],
    'photos': [{
      'photoId': '1',
      'thumbUrl': './assets/img/switzerland-2014/1.jpg',
      'url': './assets/img/switzerland-2014/1.jpg',
      'name': 'swtz1',
      'progress': '100'
    },
    {
      'photoId': '2',
      'thumbUrl': './assets/img/switzerland-2014/2.jpg',
      'url': './assets/img/switzerland-2014/2.jpg',
      'name': 'swtz2',
      'progress': '100'
    },
    {
      'photoId': '3',
      'thumbUrl': './assets/img/switzerland-2014/3.jpg',
      'url': './assets/img/switzerland-2014/3.jpg',
      'name': 'swtz3',
      'progress': '100'
    },
    {
      'photoId': '4',
      'thumbUrl': './assets/img/switzerland-2014/4.jpg',
      'url': './assets/img/switzerland-2014/4.jpg',
      'name': 'swtz4',
      'progress': '100'
    }]
  },
  {
    'galleryId': '4',
    'title': 'Egipt',
    'dateCreated': '2012-06-15',
    'thumbUrl': './assets/img/Egipt-2014/1.jpg',
    'description': 'Tym razem postanowiłem spędzieć trochę czasu w swoim rodzinnym kraju.',
    'tags': [
      { 'display': 'wakacje', 'value': 'wakacje', 'color': '#85B24B', 'tagId': '13' },
      { 'display': 'odpoczynek', 'value': 'odpoczynek', 'color': '#85B24B', 'tagId': '14' },
      { 'display': 'wodospad', 'value': 'wodospad', 'color': '#85B24B', 'tagId': '15' },
      { 'display': 'pustynia', 'value': 'pustynia', 'color': '#85B24B', 'tagId': '16' },
    ],
    'photos': [{
      'photoId': '1',
      'thumbUrl': './assets/img/Egipt-2014/1.jpg',
      'url': './assets/img/Egipt-2014/1.jpg',
      'name': 'egipt1',
      'progress': '100'
    },
    {
      'photoId': '2',
      'thumbUrl': './assets/img/Egipt-2014/2.jpg',
      'url': './assets/img/Egipt-2014/2.jpg',
      'name': 'egipt2',
      'progress': '100'
    },
    {
      'photoId': '3',
      'thumbUrl': './assets/img/Egipt-2014/3.jpg',
      'url': './assets/img/Egipt-2014/3.jpg',
      'name': 'egipt3',
      'progress': '100'
    },
    {
      'photoId': '4',
      'thumbUrl': './assets/img/Egipt-2014/4.jpg',
      'url': './assets/img/Egipt-2014/4.jpg',
      'name': 'egipt4',
      'progress': '100'
    }]
  },
  {
    'galleryId': '5',
    'title': 'Malediwy',
    'dateCreated': '2015-12-15',
    'thumbUrl': './assets/img/maldives-2014/1.jpeg',
    'description': 'Spotkałem tutaj wielu wspaniałych ludzi i odwiedziłem niesamowite miejca.',
    'tags': [
      { 'display': 'wakacje', 'value': 'wakacje', 'color': '#85B24B', 'tagId': '17' },
      { 'display': 'odpoczynek', 'value': 'odpoczynek', 'color': '#85B24B', 'tagId': '18' },
      { 'display': 'wodospad', 'value': 'wodospad', 'color': '#85B24B', 'tagId': '19' },
      { 'display': 'auto', 'value': 'auto', 'color': '#85B24B', 'tagId': '20' },
    ],
    'photos': [{
      'photoId': '1',
      'thumbUrl': './assets/img/maldives-2014/1.jpeg',
      'url': './assets/img/maldives-2014/1.jpeg',
      'name': 'male1',
      'progress': '100'
    },
    {
      'photoId': '2',
      'thumbUrl': './assets/img/maldives-2014/2.jpg',
      'url': './assets/img/maldives-2014/2.jpg',
      'name': 'male2',
      'progress': '100'
    },
    {
      'photoId': '3',
      'thumbUrl': './assets/img/maldives-2014/3.jpg',
      'url': './assets/img/maldives-2014/3.jpg',
      'name': 'male3',
      'progress': '100'
    },
    {
      'photoId': '4',
      'thumbUrl': './assets/img/maldives-2014/4.jpg',
      'url': './assets/img/maldives-2014/4.jpg',
      'name': 'male4',
      'progress': '100'
    }]
  },
  {
    'galleryId': '6',
    'title': 'Rosja',
    'dateCreated': '2016-04-15',
    'thumbUrl': './assets/img/Rosja-2014/1.jpg',
    'description': 'Wielkie jeziora i wspaniałe lasy. Polecam wakacje w Kanadzie.',
    'tags': [
      { 'display': 'wakacje', 'value': 'wakacje', 'color': '#85B24B', 'tagId': '21' },
      { 'display': 'odpoczynek', 'value': 'odpoczynek', 'color': '#85B24B', 'tagId': '22' },
      { 'display': 'voodka', 'value': 'voodka', 'color': '#85B24B', 'tagId': '23' },
      { 'display': 'putin', 'value': 'putin', 'color': '#85B24B', 'tagId': '24' },
    ],
    'photos': [{
      'photoId': '1',
      'thumbUrl': './assets/img/Rosja-2014/1.jpg',
      'url': './assets/img/Rosja-2014/1.jpg',
      'name': 'russia1',
      'progress': '100'
    },
    {
      'photoId': '2',
      'thumbUrl': './assets/img/Rosja-2014/2.jpg',
      'url': './assets/img/Rosja-2014/2.jpg',
      'name': 'russia2',
      'progress': '100'
    },
    {
      'photoId': '3',
      'thumbUrl': './assets/img/Rosja-2014/3.jpg',
      'url': './assets/img/Rosja-2014/3.jpg',
      'name': 'russia3',
      'progress': '100'
    },
    {
      'photoId': '4',
      'thumbUrl': './assets/img/Rosja-2014/4.jpg',
      'url': './assets/img/Rosja-2014/4.jpg',
      'name': 'russia4',
      'progress': '100'
    }]
  }
];
