export default [
  {
    'id': '1',
    'title': 'Jak przygotować się na pierwszą wizytę?',
    // tslint:disable-next-line:max-line-length
    'description': 'Na pierwszą wizytę proszę wziąć wszystkie wyniki badań obrazowych, laboratoryjne oraz wypisy ze szpitala. Należy wziąć ze sobą również wygodny strój sportowy (najlepiej krótkie spodenki i koszulka na ramiączkach, aby było wygodnie i komfortowo.',
    'photos': [{
      'photoId': '1',
      'thumbUrl': './assets/img/biegun-2014/1.jpg',
      'imgUrl': './assets/img/biegun-2014/1.jpg',
    }]
  }, {
    'id': '2',
    'title': 'Czy warto zrobić wszystkie badania przed pierwszą wizytą?',
    // tslint:disable-next-line:max-line-length
    'description': 'Nie jest to konieczne. Fizjoterpapeuta ma wiele testów i badań do dyspozycji, aby sprawdzić, gdzie jest przyczyna bólu. Jeśli takie badania będą konieczne fizjoterapeuta odeśle Cię do odpowiedniego specjalisty po konsultacje lub na wykonanie specjalistycznego badania.',
    'photos': [{
      'photoId': '1',
      'thumbUrl': './assets/img/biegun-2014/1.jpg',
      'imgUrl': './assets/img/biegun-2014/1.jpg',
    }]
  }, {
    'id': '3',
    'title': 'Czy warto iść do fizjoterapeuty jeśli nic mnie nie boli?',
    // tslint:disable-next-line:max-line-length
    'description': 'Zdecydowanie powinieneś to zrobić. Pamiętaj o tym, że lepiej zapobiegać niż leczyć! Każdy ma jakieś nawyki ruchowe czy przeciążenia spowodowane swoją pracą zawodową. Warto swoje ciało regularnie poddawać przeglądowi i regeneracji, żeby nie dopuścić do kumulacji przeciążeń. Pamiętajmy, że ból jest to informacja, że coś już jest mocno nie tak w naszym organizmie. Reagujmy zatem zanim ten bol wystąpi oszczędzając w ten sposób sobie negatywnych wrażeń i poprawiając swoje samopoczucie fizyczne i sprawność. Nie zapominajmy również o psychicznym aspekcie zabiegów fizjoterapii. Pozwalaja one uwolnić się od stresu, który obciąża w mniejszym lub większym stopniu każdego z nas.',
    'photos': [{
      'photoId': '1',
      'thumbUrl': './assets/img/biegun-2014/1.jpg',
      'imgUrl': './assets/img/biegun-2014/1.jpg',
    }]
  }, {
    'id': '4',
    // tslint:disable-next-line:max-line-length
    'title': 'Czy  warto się wybrać do fizjoterapeuty z bólami głowy, bolesnymi miesiączkami, bólami kolkowymi, zaparciami, bólami kłującymi w klatce piersiowej? ',
    // tslint:disable-next-line:max-line-length
    'description': 'Oczywiście bóle głowy w większości przypadków można wyleczyć za pomocą terapii układu ruchu. Typowe migrenowe bóle głowy niereagujące na terapie występują bardzo rzadko. Duże znaczenie odegra różnicowanie bólu głowyw podczas wywiadu i badania manualnego. Bolesne miesiączki, zaparcia, bóle kolkowe to problemy wynikające bardzo często z przeciążenia więzadeł oraz zaburzeń tkankowych w obrębie jamy brzusznej, z którymi fizjoterapeuta może sobie poradzić pracą manualną. Bóle klatki piersiowej to objaw nieprawidłowej pracy więzadeł, przepony czy sztywności żeber i całej klatki piersiowej.',
    'photos': [{
      'photoId': '1',
      'thumbUrl': './assets/img/biegun-2014/1.jpg',
      'imgUrl': './assets/img/biegun-2014/1.jpg',
    }]
  }, {
    'id': '5',
    'title': 'Ile wizyt jest potrzebnych do osiągnięcia zamierzonego rezultatu?',
    // tslint:disable-next-line:max-line-length
    'description': 'Każdy z nas może mieć inną reakcję na ten sam zabieg, w zależności od tego dobierane są odstepy niezbędne między zabiegami, aby organizm zaakceptował zmianę zastosowaną w trakcie zabiegu. Ilość wizyt jest zależna od tego jaki mamy cel. Do usunięcia bólu często wywstarczą 1-3 wizyty, natomiast do zauwważalnej zmiany naszej postawy ciała potrzeba około pół roku bo tyle trwa strukturalna przebudowa tkanek i ich adaptacja.',
    'photos': [{
      'photoId': '1',
      'thumbUrl': './assets/img/biegun-2014/1.jpg',
      'imgUrl': './assets/img/biegun-2014/1.jpg',
    }]
  }, {
    'id': '6',
    'title': 'Czy fizjoterapia jest w stanie poprawić moje osiągi sportowe? Siłownia/bieganie/',
    // tslint:disable-next-line:max-line-length
    'description': 'Zdecydowanie tak! Ograniczone zakresy  ruchu w stawach, asymetrie w naszym ciele, napięcia mięśniowe, zmęczenie, zaburzenie w odpowiedniej aktywacji mięśni, nieprawidłowa biomechanika ruchu i technika wykonywania ćwiczeń potrafią znacznie zmniejszyć osiągi a długotrwale prowadzą do kontuzji. Trenując na siłowni warto znać pewne detale, które usprawnią Twój progres i zabezpieczą Twoje stawy i mięśnie- jeśli zerwiesz mięsień dwugłowy czy piersiowy Twoja praca, którą włożysz w jego budowę nie będzie miała znaczenia, gdyż zerwany mięsień nigdy nie wróci już do pełni możliwości.',
    'photos': [{
      'photoId': '1',
      'thumbUrl': './assets/img/biegun-2014/1.jpg',
      'imgUrl': './assets/img/biegun-2014/1.jpg',
    }]
  }, {
    'id': '7',
    'title': 'Dla kogo dedykowane są wkładki do obuwia?',
    // tslint:disable-next-line:max-line-length
    'description': 'Wkładki  najbardziej potrzebne są osobom, które mają pracę stojącą, sportowcom – zwłaszcza w sportach z dużą komponenetą biegu, osobom aktywnym fizycznie, które dużo się przemieszczają.  Częste zastosowanie to korekcja wad postawy, bóle stóp, pięt, kolan, bioder, kręgosłupa.  Tak naprawdę wkładka będzie miała dobre zastosowanie u każdego, gdyż działa na nas siła Coriolisa, która w naszym ciele pozostawia wiele kompensacji np. zmianę ustawienia kości krzyżowej, za którą podąża całe nasze ciało wywołując różne przemieszczenia na wielu poziomach.  Podczas ruchu zwiększa się ona 6 krotnie. Jesteśmy w stanie ją zniwelować terapią manualną oraz odpowiednią wkładką, która zatrzyma skręt miednicy. ',
    'photos': [{
      'photoId': '1',
      'thumbUrl': './assets/img/biegun-2014/1.jpg',
      'imgUrl': './assets/img/biegun-2014/1.jpg',
    }]
  }, {
    'id': '8',
    'title': 'Czy bóle promieniujące do ręki i nogi to zawsze problem dyskopatyczny?',
    // tslint:disable-next-line:max-line-length
    'description': 'Niejednokrotnie słyszę , że przyczyną rwy kulszowej jest konflikt korzeniowy z dyskiem. Po czym praca na pośladku lub łydce, stopie czy w dole biodrowym usuwa objawy sławnej rwy kulszowej. Cieśń nadgarstka znika po pracy na splocie ramiennym czy na przegrodach między mięśniowych w okolicy ramienia.',
    'photos': [{
      'photoId': '1',
      'thumbUrl': './assets/img/biegun-2014/1.jpg',
      'imgUrl': './assets/img/biegun-2014/1.jpg',
    }]
  }, {
    'id': '9',
    'title': 'Czy choroba zwyrodnieniowa jest przyczyną bólu?',
    // tslint:disable-next-line:max-line-length
    'description': 'Bardzo rzadko! Masz 30 lat i dokucza Ci „zwyrodnienie”? Zwyrodnienia to naturalny proces starzenia się. Zwyrodnienia muszą być zaawansowane lub osteofit czyli narośl kostna musi uciskać nerw, aby dawały objawy silnego bólu. Jeżeli nie jesteś otyły i nie masz poważnych zaburzeń wad postawy prawdopodobnie przyczyną nie jest zwyrodnienie tylko co innego i duże prawdopodobieństwo, że można to wyleczyć, a nie trzeba „przyzwyczajać się do bólu”.',
    'photos': [{
      'photoId': '1',
      'thumbUrl': './assets/img/biegun-2014/1.jpg',
      'imgUrl': './assets/img/biegun-2014/1.jpg',
    }]
  }, {
    'id': '10',
    'title': 'Dlaczego ból mnie budzi w nocy?',
    // tslint:disable-next-line:max-line-length
    'description': 'dopisać....',
    'photos': [{
      'photoId': '1',
      'thumbUrl': './assets/img/biegun-2014/1.jpg',
      'imgUrl': './assets/img/biegun-2014/1.jpg',
    }]
  },
];
