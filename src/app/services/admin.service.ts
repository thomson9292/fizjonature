import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  ifLogged;
  ifLoggedChange: Subject<boolean> = new Subject<boolean>();

  constructor() {
    firebase.auth().onAuthStateChanged(firebaseUser => {
      if (firebaseUser) {
        this.ifLogged = true;
        this.ifLoggedChange.next(this.ifLogged);
      } else {
        this.ifLogged = false;
        this.ifLoggedChange.next(this.ifLogged);
      }
    });

    this.ifLoggedChange.subscribe((value) => {
      this.ifLogged = value;
    });
  }

  toggleIfLogged(value) {
    this.ifLoggedChange.next(value);
  }
}
