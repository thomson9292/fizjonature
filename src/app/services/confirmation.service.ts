import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfirmationService {

  data;
  ifRemove = false;
  modalOn = false;
  text;
  modalOnChange: Subject<any[]> = new Subject<any>();
  ifRemoveChange: Subject<boolean> = new Subject<boolean>();

  constructor() {
    this.modalOnChange.subscribe((value) => {
      this.modalOn = value[0];
      this.text = value[1];
    });

    this.ifRemoveChange.subscribe((value) => {
      this.modalOn = value;
    });
  }

  toggleModalOn(text) {
    this.text = text;
    this.modalOnChange.next([!this.modalOn, this.text]);
  }

  toggleIfRemove() {
    this.ifRemoveChange.next(!this.ifRemove);
  }

}
