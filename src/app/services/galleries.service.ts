import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore } from 'angularfire2/firestore';
import { IGallery } from '../interfaces/igallery';
import { map } from 'rxjs/operators';
import { Icomment } from '../interfaces/icomment';
import { Inews } from '../interfaces/inews';
import { Itags } from '../interfaces/itags';


@Injectable()
export class GalleriesService {

  galleriesObservable: Observable<any>;
  commentsObservable: Observable<any>;
  tagsToUpdate: Itags[] = [];

  constructor(private db: AngularFirestore) { }


  getGalleries(collectionName) {
    this.galleriesObservable = this.db.collection(collectionName, ref => ref.orderBy('dateCreated', 'desc')).
      snapshotChanges().pipe(
        map(changes => changes.map(a => {
          const data = a.payload.doc.data() as IGallery;
          data.galleryId = a.payload.doc.id;
          this.setId(data.galleryId, 'galleries');
          return data;
        })
        )
      );
    return this.galleriesObservable;
  }


  getSpecificGallery(id, collectionName) {
    return this.db.collection(collectionName).doc(id).valueChanges();
  }


  getNews(collectionName) {
    this.galleriesObservable = this.db.collection(collectionName, ref => ref.orderBy('dateCreated', 'desc')).
      snapshotChanges().pipe(
        map(changes => changes.map(a => {
          const data = a.payload.doc.data() as Inews;
          data.newsId = a.payload.doc.id;
          this.setId(data.newsId, 'news');
          return data;
        })
        )
      );
    return this.galleriesObservable;
  }


  getTags(collectionName) {
    this.galleriesObservable = this.db.collection(collectionName).
      snapshotChanges().pipe(
        map(changes => changes.map(a => {
          console.log('pobieram tagi');
          const data = a.payload.doc.data() as Itags;
          data.tagId = a.payload.doc.id;
          this.setId(data.tagId, 'tags');
          return data;
        })
        )
      );
    return this.galleriesObservable;
  }


  getComments(collectionName, id?) {
    // return this.db.collection(collectionName + id)..valueChanges();
    this.commentsObservable = this.db.collection(collectionName, ref => ref.orderBy('dateCreated', 'desc')).
      snapshotChanges().pipe(
        map(changes => changes.map(a => {
          const data = a.payload.doc.data() as Icomment;
          data.commentId = a.payload.doc.id;
          this.setId(data.commentId, 'comments');
          return data;
        })
        )
      );
    return this.commentsObservable;
  }

  updateColors(collectionName, tagsToUpdate) {
    // console.log(tagsToUpdate);
    this.db.collection(collectionName, ref => ref.orderBy('dateCreated', 'desc')).
      snapshotChanges().pipe(
        map(changes => changes.map(a => {
          const data = a.payload.doc.data() as IGallery;
          data.galleryId = a.payload.doc.id;
          this.setId(data.galleryId, 'galleries');
          return data;
        })
        )
      ).take(1)
      .subscribe(items => {
        this.db.collection(collectionName, ref => ref.orderBy('dateCreated', 'desc')).snapshotChanges();
        items.forEach(gallery => {
          this.tagsToUpdate = [];
          gallery.tags.forEach((oldTag, index) => {
            tagsToUpdate.forEach(newTag => {
              if (oldTag.value === newTag.value) {
                console.log(oldTag.value + ' - ' + newTag.value, index);
                this.tagsToUpdate.push(newTag);
              }
            });
          });
          return this.db.collection(collectionName).doc(gallery.galleryId).set({ 'tags': this.tagsToUpdate }, { merge: true });
        });
      });
  }


  sendData(data, collectionName) {
    return this.db.collection(collectionName).add(data);
  }


  setId(id, collectionName) {
    if (collectionName === 'galleries') {
      return this.db.collection(collectionName).doc(id).update({
        galleryId: id
      });
    } else if (collectionName === 'comments') {
      return this.db.collection(collectionName).doc(id).update({
        commentId: id
      });
    } else if (collectionName === 'news') {
      return this.db.collection(collectionName).doc(id).update({
        newsId: id
      });
    } else if (collectionName === 'tags') {
      return this.db.collection(collectionName).doc(id).update({
        tagId: id
      });
    }
  }


  updateData(data, collectionName, doc): any {
    console.log('Data to update - provided to service ', data);
    return this.db.collection(collectionName).doc(doc).update(data);
  }


  delSingleData(id: string, collectionName): any {
    return this.db.collection(collectionName).doc(id).delete().then(e => {
      console.log();
    });
  }

}
