import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Icomment } from '../interfaces/icomment';

@Injectable()
export class CommentsService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': '20'
    })
  };

  comment: Icomment;
  comments: Icomment[] = [];
  galleryId: string;

  constructor(private route: ActivatedRoute, private http: HttpClient) {
  }

  // pobranie komentow z bazy
  public getComment(galleryId, comments) {
    return this.http.get('http://project.usagi.pl/comment/byGallery/' + galleryId, this.httpOptions);
  }



  ////////////////////////////////////////////////////////
  //// ToDo - zrobić serwis z pełną obsługą komentarzy ///
  ////////////////////////////////////////////////////////

  // // pobranie komentow z bazy
  // public getComment() {
  //   this.galleryId = this.route.snapshot.paramMap.get('galleryId');
  //   this.http.get('http://project.usagi.pl/comment/byGallery/' + this.galleryId, this.httpOptions).toPromise().then((response) => {
  //     this.comments = <any>response;
  //   });
  // }


  // // dodanie komenta do bazy
  // onSubmit(comment, done?: any, err?: any) {
  //   this.http.post('http://project.usagi.pl/comment', comment,
  //     this.httpOptions).toPromise().then((response) => {
  //       // this.toastr.success('Comment added!');
  //       this.comments.push(<any>response);
  //     }, (errResponse) => {
  //       // this.toastr.error('Something went wrong!');
  //     });
  // }


  // // usuwanie komenta
  // onDelete(commentId) {
  //   const index = this.comments.findIndex(item => item.commentId ===
  //     commentId);
  //   this.http.post('http://project.usagi.pl/comment/delete/' +
  //     commentId, {}, this.httpOptions).toPromise().then(() => {
  //       this.comments.splice(index, 1);
  //       this.toastr.success('Comment removed');
  //     }, (errResponse) => {
  //       this.toastr.error('Something went wrong!');
  //     });
  // }

}
