import { Injectable } from '@angular/core';
import * as uuid from 'uuid/v4';
import { Itags } from '../interfaces/itags';

@Injectable({
  providedIn: 'root'
})
export class TagsService {

  newUniqueTags: Itags[] = [];

  constructor() { }


  newTag(name) {
    return {
      tagId: uuid(),
      display: name,
      value: name,
      color: '#85B24B',
    };
  }


  setNewListOfTags(gallery, uniqueTags) {
    this.newUniqueTags = [];
    gallery.tags.forEach((element, index) => {
      element = element.value;
      let duplicate = 0;
      let check = 0;
      console.log(uniqueTags);
      uniqueTags.forEach(tag => {
        if (tag['value'].indexOf(element) === -1) {
          check = check + 1;
          if (check === uniqueTags.length && duplicate !== 1) {
            this.newUniqueTags.push(this.newTag(element));
            gallery.tags[index] = this.newTag(element);
            console.log('SPRAWDZONY! - ' + check + ' / ' + uniqueTags.length + ' (' + duplicate + ')');
          }
        } else if (tag['value'].indexOf(element) !== -1) {
          gallery.tags[index] = tag;
          duplicate = 1;
        }
      });
      if (uniqueTags.length < 1) {
        this.newUniqueTags.push(this.newTag(element));
        gallery.tags[index] = this.newTag(element);
      }
    });
    return [gallery, this.newUniqueTags];
  }

}
