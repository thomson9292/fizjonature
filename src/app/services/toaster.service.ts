import { ToastrService } from 'ngx-toastr';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ToasterAppService {

  constructor(private toastr: ToastrService) {
  }

  Success(title: string, message?: string) {
    this.toastr.success(title, message);
    console.log('success');
  }

  Error(title: string, message?: string) {
    this.toastr.error(title, message);
  }

  Info(title: string, message?: string) {
    this.toastr.info(title, message);
  }

}
