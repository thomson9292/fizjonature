import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import GALLERY from '../constants/galleries';
import { DatePipe } from '@angular/common';

@Injectable()
export class LastYearService {

  yearList: Date[] = [];
  galleries: any;
  lastTrip: Date;
  lastGalleries: any;

  private lastGallery = new Subject<any>();

  constructor(private datePipe: DatePipe) {

    this.galleries = GALLERY;

    // lista roczników + usuwanie duplikatów
    this.galleries.forEach(element => {
      element = datePipe.transform(element['dateCreated'], 'yyyy');
      if (this.yearList.indexOf(element) === -1) {
        this.yearList.push(element);
      }
    });
    this.yearList.sort();

    this.lastTrips();

    this.lastGallery.next(this.lastGalleries);
  }

  sendGallery(): Observable<any> {
    console.log('send Observable');
    return this.lastGallery.asObservable();
  }


  public lastTrips() {
    this.lastTrip = this.yearList[this.yearList.length - 1];
    this.galleries = this.galleries.filter(item =>
      item.dateCreated.indexOf(this.lastTrip) !== -1);
    this.lastGalleries = this.galleries;
  }

}
