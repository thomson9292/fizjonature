import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
// import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from 'angularfire2/storage';
import { Observable, Subject } from 'rxjs';
import { Upload } from '../interfaces/upload';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {


  downloadURL: Observable<string>;
  uploadPercent: Observable<number>;

  private uploadTask: firebase.storage.UploadTask;

  photoLink;
  url;

  constructor() { }

  removeData(key, path) {
      const storageRef = firebase.storage().ref();
      storageRef.child(path + key).delete();
  }

  uploadData(upload: Upload, licznik, path): Promise<any> {
    return new Promise((resolve, reject) => {
      const storageRef = firebase.storage().ref();
      console.log('licznik ' + licznik);
      this.uploadTask = storageRef.child(path + upload.$key).put(upload.file);

      this.uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot) => {
          upload.progress = (this.uploadTask.snapshot.bytesTransferred / this.uploadTask.snapshot.totalBytes) * 100;
          console.log(upload.progress);
        },
        (error) => {
          console.log(error);
        },
        () => {
          console.log(this.uploadTask.snapshot);
          console.log('licznik w trakcie pobieraniu URLa ' + licznik);
          this.url = this.uploadTask.snapshot.ref.getDownloadURL()
            .then(downloadURL => {
              setTimeout(() => {
                this.photoLink = downloadURL;
                console.log('Successfully uploaded file and got download link -' + downloadURL);
                resolve(this.photoLink);
              }, 2000);
            })
            .catch(error => {
              console.log('Failed to upload file and get link ' + error);
            });
        });
    });
  }

}
